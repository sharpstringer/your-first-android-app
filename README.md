# README #

### What is this repository for? ###

* A very simple Android example app
* Version 1.0

### How do I get set up? ###

* Get set up by following the tutorial at http://rowanstringer.com/your-first-android-app
* This source code is meant as a reference, to ensure that your final output from the tutorial matches the intended output

### Who do I talk to? ###

* For any errors or inconsistencies please contact me via http://rowanstringer.com/getintouch